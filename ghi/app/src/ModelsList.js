import React from 'react';
import { NavLink } from 'react-router-dom'

export default function ModelsList({ vehicleList }) {
    return (
        <>
        <h1 className="col-lg-4 bdr mx-auto bg-dark mt-5 p-4 text-center" style={{color: "white"}}>All Manufacturers</h1>
        <table className="table bdr table-hover table-info table-dark mt-2">
            <thead class="thead-dark">
                    <tr>
                        <th>Model Name</th>
                        <th>Manufacturer</th>
                        <th className="text-center">Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleList && vehicleList.map(model => {
                        return (
                            <tr key={model.href}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <div className="center-image">
                                    <td><img src={model.picture_url} alt="" width="100%" height="auto" /></td>
                                </div>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <button type="button" className=" btn btn-dark btn-sm float-end" >
        <NavLink className="fs-6" aria-current="page" to="/models/new/" style={{color: "white"}}>Add a Model</NavLink>
      </button>
        </>
    )
}
