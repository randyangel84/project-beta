import React, { useEffect, useState } from "react";
import { NavLink } from 'react-router-dom'

function ServiceHistory({AppointmentList}) {
    const [search, setSearch] = useState("");
    useEffect(() => { }, []);

    return (
        <>
            <h1 className="bdr mx-auto bg-dark mt-5 text-center w-50 p-3" style={{color: "white"}}>Service History by VIN</h1>
            <div className="container">
                <div className="row">
                    <form id="form_search" name="form_search" method="get" action="" className="form-inline">
                    <div className="form-group p-3 mb-2 bg-dark text-white">
                            <div className="input-group p-3 mb-2 bg-dark text-white" >
                                <input onChange={event => setSearch(event.target.value)} className="form-control" type="text" placeholder="Search by Vin" />
                            </div>
                        </div>
                    </form>
                </div>
                <table className="table bdr table-hover table-info table-dark mt-2">
            <thead class="thead-dark">
                <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {AppointmentList && AppointmentList
                .filter(history => history.vin.includes(search))
                .map( history => {
                    return (
                    <tr key={history.id}>
                        <td>{history.vin}</td>
                        <td>{history.customer_name}</td>
                        <td>{history.date}</td>
                        <td>{history.time}</td>
                        <td>{history.technician}</td>
                        <td>{history.reason}</td>
                    </tr>
                    );
                })}
            </tbody>
            </table>
            </div>
            <button className="btn btn-dark bdr float-end">
            <NavLink className="fs-6" aria-current="page" to="/automobiles/new/" style={{color: "white"}}>Create Automobile (VIN)</NavLink>
            </button>
        </>
  );
}

export default ServiceHistory;
