import React from 'react';
import { NavLink } from 'react-router-dom'

export default function ManufacturersList({ manuList }) {
    return (
        <>
        <h1 className="col-lg-4 p-4 bdr mx-auto bg-dark mt-5 text-center" style={{color: "white"}}>All Manufacturers</h1>
            <table className="table bdr table-hover table-info table-dark mt-2">
            <thead class="thead-dark">
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {manuList && manuList.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <button type="button" className=" btn btn-dark btn-sm float-end" >
        <NavLink className="fs-6" aria-current="page" to="/manufacturers/new/" style={{color: "white"}}>Add a Manufacturer</NavLink>
      </button>
        </>
    )
}