function MainPage() {
  return (
    <div className="px-4 py-5 my-2 text-center">
      <h1 className="btn btn-outline-light col-lg-3 bdr  bg-dark p-3  text-decoration-underline" style={{color: "white"}}>CarCar 🏎️🚗🚙</h1>
      <div className=" mx-auto  ">
        <p className="btn btn-outline-light  bdr  bg-dark  w-75 p-4   " style={{color: "white", fontWeight: '3000'}}>
          The premiere solution for automobile dealership 
          management! 
          <br></br><br></br>
          <p>🔥</p>
          <p className=" text-decoration-underline">🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️🏎️</p>
          <p className=" text-decoration-underline">🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗</p>
          <p className=" text-decoration-underline">🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙🚙</p>
          <p>🔥</p>
          Created by Randy Angel & Vincent Lee
        </p>
      </div>
    </div>
  );
}

export default MainPage;
