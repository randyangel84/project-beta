import React from 'react'
import { NavLink } from 'react-router-dom'

export default function SalesRecordList({ salesRecords }) {
    return (
        <>
        <h1 className="col-lg-4 bdr mx-auto bg-dark mt-5 p-3 text-center" style={{color: "white"}}>Sales Records</h1>
            <table className="table bdr table-hover table-info table-dark mt-2">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Sales Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesRecords && salesRecords.map(record => {
                        return (
                            <tr key={record.id}>
                                <td>{record.sales_person.name}</td>
                                <td>{record.customer.name}</td>
                                <td>{record.vin}</td>
                                <td>{record.sales_price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <button type="button" className=" btn btn-dark btn-sm float-end" >
        <NavLink className="fs-6" aria-current="page" to="/sales-records/new/" style={{color: "white"}}>Create Sales Record</NavLink>
      </button>
        </>
    )
}
