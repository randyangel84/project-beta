from django.urls import path

from .views import (
    list_techicians,
    detail_technician,
    list_appointments,
    detail_appointment
)

urlpatterns =[
    path("technicians/", list_techicians, name="list_technicians"),
    path("technician/<int:pk>/", detail_technician, name="detail_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:pk>/", detail_appointment, name="detail_appointment"),
]