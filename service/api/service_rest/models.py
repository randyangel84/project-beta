from django.db import models

# Create your models here.

class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=100)
    date = models.CharField(max_length=100)
    time = models.CharField(max_length=100)
    reason = models.CharField(max_length=144)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
        )
    finished = models.BooleanField(default=False)

    def __str__(self):
        return f'{str(self.customer_name), str(self.reason)}'


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
